
import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import SecondPage from "./pages/SencondPage";
import ThirdPage from "./pages/ThirdPage";

const routerList = [
    {path: "/", element: <HomePage/>},
    {label: "FirstPage", path: "/firstpage", element: <FirstPage/>},
    {label: "SecondPage", path: "/secondpage/:pageid", href: "/secondpage/12", element: <SecondPage/>},
    {label: "SecondPage to ThirdPage", href: "/secondpage/third", element: <SecondPage/>},
    {label: "ThirdPage", path: "/thirdpage", element: <ThirdPage/>},
]
export default routerList;