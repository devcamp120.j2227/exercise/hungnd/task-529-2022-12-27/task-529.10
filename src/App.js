import Content from "./views/Content";
import Footer from "./views/Footer";
import Header from "./views/Header";
import './App.css';
function App() {
  return (
    <div>
      <Header/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
