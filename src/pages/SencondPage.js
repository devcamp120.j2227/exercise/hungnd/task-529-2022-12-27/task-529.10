import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

const SecondPage = () => {
    const navigate = useNavigate();
    const { pageid } = useParams();
    
    useEffect(() => {
        if(pageid === "third") {
            navigate("/thirdpage")
        }
    })
    return (
        <div>
            {
                pageid ?
                "SecondPage with id: " + pageid :
                "SecondPage"
            }
        </div>
    )
}
export default SecondPage;