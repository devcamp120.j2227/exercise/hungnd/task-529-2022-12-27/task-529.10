import routes from '../routes';
import { Routes, Route } from "react-router-dom";
import ThirdPage from '../pages/ThirdPage';

const Content = () => {
    return (
        <div style={{ marginTop: "30px" }}>
            <Routes>
                {routes.map((router, index) => {
                    if (router.path) {
                        return <Route key={index} exact path={router.path} element={router.element}></Route>
                    }
                    else {
                        return null;
                    }

                })}
                <Route path='*' element={<ThirdPage/>}></Route>
            </Routes>
        </div>
    )
}
export default Content;